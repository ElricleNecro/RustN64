use std::error;
use std::fmt::{self,Display,Formatter,Result};

#[derive(Debug)]
pub enum ErrorKind {
    UnsupportedInstruction,
    InvalidMemory,
    InvalidWrite,
    InvalidRead,
}

pub struct Error {
    descr: String,
    kind: ErrorKind,
}

impl Error {
    pub fn new(msg: &str, kind: ErrorKind) -> Self {
        Error {
            descr: msg.to_string(),
            kind: kind,
        }
    }
}

impl Display for Error {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        match self.kind {
            ErrorKind::UnsupportedInstruction => {
                write!(fmt, "Unsupported instruction: {}", self.descr)
            },

            ErrorKind::InvalidWrite => {
                write!(fmt, "Invalid write access: {}", self.descr)
            },

            ErrorKind::InvalidRead => {
                write!(fmt, "Invalid read access: {}", self.descr)
            },

            ErrorKind::InvalidMemory => {
                write!(fmt, "Invalid memory address: {}", self.descr)
            },
        }
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        match self.kind {
            ErrorKind::UnsupportedInstruction => {
                write!(fmt, "Unsupported instruction: {}", self.descr)
            },

            ErrorKind::InvalidWrite => {
                write!(fmt, "Invalid write access: {}", self.descr)
            },

            ErrorKind::InvalidRead => {
                write!(fmt, "Invalid read access: {}", self.descr)
            },

            ErrorKind::InvalidMemory => {
                write!(fmt, "Invalid memory access: {}", self.descr)
            },
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        &self.descr
    }
}
