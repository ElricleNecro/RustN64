/// Model system bus
use std::fmt;

// TODO: remove this dependency to do it ourselves.
use byteorder::{BigEndian,ByteOrder};

use super::errors::{Error, ErrorKind};
use super::mem_map::*;
use super::rsp::Rsp;
use super::peripheral_interface::PeripheralInterface;

pub struct Interconnect {
    pif_rom: Box<[u8]>,
    ram: Box<[u16; RAM_SIZE]>,

    rsp: Rsp,

    pi: PeripheralInterface,
}

impl Interconnect {
    pub fn new(pif: Box<[u8]>) -> Interconnect {
        Interconnect {
            pif_rom: pif,
            ram: box [0; RAM_SIZE],

            rsp: Default::default(),
            pi:  Default::default(),
        }
    }

    pub fn write_word(&mut self, addr: u32, word: u32) -> Result<(), Error> {
        match mem_addr(addr)? {
            AddrType::PifRom(_) => Err(Error::new(&format!("Invalid write access: trying to write in PIF rom ({:#x}).", addr), ErrorKind::InvalidWrite)),
            AddrType::SpStatusReg => Ok(self.rsp.write_status(word)),
            AddrType::SpDmaBusyReg => Ok(self.rsp.write_dma_busy_reg(word)),
            AddrType::PiStatusReg => Ok(self.pi.write_status_reg(word)),
        }
    }

    pub fn read_word(&self, addr: u32) -> Result<u32, Error> {
        // TODO: Maybe these values should be constants:
        match mem_addr(addr)? {
            AddrType::PifRom(offset) => Ok( BigEndian::read_u32(&self.pif_rom[offset as usize..]) ),
            AddrType::SpStatusReg => Ok(self.rsp.read_status()),
            AddrType::SpDmaBusyReg => Ok(self.rsp.read_dma_busy_reg()),
            AddrType::PiStatusReg => Ok(self.pi.read_status_reg()),
        }
    }
}

            // TODO: Compare with how byteorder crate work:
            // ((self.pif_rom[rel_addr] as u32) << 24) |
            // ((self.pif_rom[rel_addr + 1] as u32) << 16) |
            // ((self.pif_rom[rel_addr + 2] as u32) << 8) |
            // (self.pif_rom[rel_addr + 3] as u32)

impl fmt::Debug for Interconnect {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Memory of size {}.", RAM_SIZE)
    }
}
