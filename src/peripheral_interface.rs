#[derive(Default)]
pub struct PeripheralInterface {
}

impl PeripheralInterface {
    pub fn write_status_reg(&mut self, word: u32) {
        if (word & (1 << 0)) != 0 {
            println!("WARNING: PI reset controller not implemented!");
        }

        if (word & (1 << 1)) != 0 {
            // TODO: Affect MI_INTR_REG
            println!("WARNING: PI clear intr not implemented!");
        }
    }

    pub fn read_status_reg(&self) -> u32 {
        unimplemented!()
    }
}
