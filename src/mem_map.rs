use super::errors::{Error, ErrorKind};

const PIF_ROM_START: u32 = 0x1fc0_0000;
const PIF_ROM_SIZE : u32 = 0x0000_07c0;
const PIF_ROM_END  : u32 = PIF_ROM_START + PIF_ROM_SIZE;
const PIF_ROM_END_E: u32 = PIF_ROM_END - 1;

const SP_BASE_REG    : u32 = 0x0404_0000;
const SP_STATUS_REG  : u32 = 0x0404_0010;
const SP_DMA_BUSY_REG: u32 = 0x0404_0018;

const PI_BASE_REG        : u32 = 0x0460_0000;
const PI_STATUS_REG      : u32 = 0x0460_0010;

pub const RAM_SIZE: usize = 4 * 1024 * 1024;

pub enum AddrType {
    PifRom(u32),

    SpStatusReg,
    SpDmaBusyReg,

    PiStatusReg,
}

pub fn mem_addr(addr: u32) -> Result<AddrType, Error> {
    match addr {
        PIF_ROM_START...PIF_ROM_END_E => Ok(AddrType::PifRom(addr - PIF_ROM_START)),
        SP_STATUS_REG => Ok(AddrType::SpStatusReg),
        SP_DMA_BUSY_REG => Ok(AddrType::SpDmaBusyReg),
        PI_STATUS_REG => Ok(AddrType::PiStatusReg),
        _ => Err(Error::new(&format!("Unsupported memory address {:#x}.", addr), ErrorKind::InvalidMemory)),
    }
}
