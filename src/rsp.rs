pub struct Rsp {
    halt: bool,
    dma_busy: bool,
    dma_full: bool,
    io_full: bool,
    broke: bool,
    intr: bool,
    sstep: bool,
    breaks: bool,
    sig0: bool,
    sig1: bool,
    sig2: bool,
    sig3: bool,
    sig4: bool,
    sig5: bool,
    sig6: bool,
    sig7: bool,
}

impl Rsp {
    pub fn read_status(&self) -> u32 {
        (if self.halt { 1 } else { 0 }      << 0) |
        (if self.broke { 1 } else { 0 }     << 1) |
        (if self.dma_busy { 1 } else { 0 }  << 2) |
        (if self.dma_full { 1 } else { 0 }  << 3) |
        (if self.io_full { 1 } else { 0 }   << 4) |
        (if self.sstep { 1 } else { 0 }     << 5) |
        (if self.breaks { 1 } else { 0 }    << 6) |
        (if self.sig0 { 1 } else { 0 }      << 7) |
        (if self.sig1 { 1 } else { 0 }      << 8) |
        (if self.sig2 { 1 } else { 0 }      << 9) |
        (if self.sig3 { 1 } else { 0 }      << 10) |
        (if self.sig4 { 1 } else { 0 }      << 11) |
        (if self.sig5 { 1 } else { 0 }      << 12) |
        (if self.sig6 { 1 } else { 0 }      << 13) |
        (if self.sig7 { 1 } else { 0 }      << 14)
    }

    pub fn write_status(&mut self, word: u32) {
        if (word & (1 << 0)) != 0 {
            self.halt = false;
        }

        if (word & (1 << 1)) != 0 {
            self.halt = true;
        }

        if (word & (1 << 2)) != 0 {
            self.broke = true;
        }

        if (word & (1 << 3)) != 0 {
            self.intr = false;
        }

        if (word & (1 << 4)) != 0 {
            self.intr = true;
        }

        if (word & (1 << 5)) != 0 {
            self.sstep = false;
        }

        if (word & (1 << 6)) != 0 {
            self.sstep = true;
        }

        if (word & (1 << 7)) != 0 {
            self.breaks = false;
        }

        if (word & (1 << 8)) != 0 {
            self.breaks = true;
        }

        if (word & (1 << 9)) != 0 {
            self.sig0 = false;
        }

        if (word & (1 << 10)) != 0 {
            self.sig0 = true;
        }

        if (word & (1 << 11)) != 0 {
            self.sig1 = false;
        }

        if (word & (1 << 12)) != 0 {
            self.sig1 = true;
        }

        if (word & (1 << 13)) != 0 {
            self.sig2 = false;
        }

        if (word & (1 << 14)) != 0 {
            self.sig2 = true;
        }

        if (word & (1 << 15)) != 0 {
            self.sig3 = false;
        }

        if (word & (1 << 16)) != 0 {
            self.sig3 = true;
        }

        if (word & (1 << 17)) != 0 {
            self.sig4 = false;
        }

        if (word & (1 << 18)) != 0 {
            self.sig4 = true;
        }

        if (word & (1 << 19)) != 0 {
            self.sig5 = false;
        }

        if (word & (1 << 20)) != 0 {
            self.sig5 = true;
        }

        if (word & (1 << 21)) != 0 {
            self.sig6 = false;
        }

        if (word & (1 << 22)) != 0 {
            self.sig6 = true;
        }

        if (word & (1 << 23)) != 0 {
            self.sig7 = false;
        }

        if (word & (1 << 24)) != 0 {
            self.sig7 = true;
        }
    }

    pub fn write_dma_busy_reg(&mut self, word: u32) {
        if (word & (1 << 0)) != 0 {
            self.dma_busy = true;
        }
    }

    pub fn read_dma_busy_reg(&self) -> u32 {
        if self.dma_busy {
            1
        } else {
            0
        }
    }
}

impl Default for Rsp {
    fn default() -> Self {
        Rsp {
            halt   : true,
            dma_busy: false,
            dma_full: false,
            io_full: false,
            broke  : true,
            intr   : false,
            sstep  : false,
            breaks : false,
            sig0   : false,
            sig1   : false,
            sig2   : false,
            sig3   : false,
            sig4   : false,
            sig5   : false,
            sig6   : false,
            sig7   : false,
        }
    }
}
