mod cp0;
pub(crate) mod cpu;
mod reg_config;
mod reg_status;
mod instruction;
mod opcode;

pub(crate) use self::cpu::Cpu;
