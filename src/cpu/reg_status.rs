#[derive(Debug,Default)]
struct DiagnosticStatus {
    /// Instruction trace support:
    its: bool,

    /// TLB miss and general purpose exception (true: Normal, false: Bootstrap):
    /// TODO: Use an enum?
    bev: bool,

    /// Indicates TLB shutdown has occured:
    ts: bool,

    /// Soft reset or NMI occured:
    sr: bool,

    /// Condition bit:
    ch: bool,
}

impl DiagnosticStatus {
    fn configure(&mut self, data: u32) {
        self.its = (data & (1 << 24)) != 0;
        self.bev = (data & (1 << 22)) != 0;
        self.ts  = (data & (1 << 21)) != 0;
        self.sr  = (data & (1 << 20)) != 0;
        self.ch  = (data & (1 << 18)) != 0;
    }
}

impl From<u32> for DiagnosticStatus {
    fn from(data: u32) -> Self {
        DiagnosticStatus {
            its: (data & (1 << 24)) != 0,
            bev: (data & (1 << 22)) != 0,
            ts : (data & (1 << 21)) != 0,
            sr : (data & (1 << 20)) != 0,
            ch : (data & (1 << 18)) != 0,
        }
    }
}

#[derive(Debug,Default)]
struct InterruptMask {
    /// Timer interrupt:
    im7: bool,

    /// External interrupt or write request:
    im62: [bool; 5],

    /// Software interrupt and cause register:
    im10: [bool; 2],
}

impl InterruptMask {
    fn configure(&mut self, data: u32) {
        self.im7 = (data & (1 << 7)) != 0;
        for i in 6..1 {
            self.im62[i] = (data & (1 << 7)) != 0;
        }

        self.im10[1] = (data & (1 << 1)) != 0;
        self.im10[0] = (data & (1 << 0)) != 0;
    }
}

impl From<u32> for InterruptMask {
    fn from(data: u32) -> Self {
        InterruptMask {
            im7:  (data & (1 << 15)) != 0,
            im62: [
                (data & (1 << 10)) != 0,
                (data & (1 << 11)) != 0,
                (data & (1 << 12)) != 0,
                (data & (1 << 13)) != 0,
                (data & (1 << 14)) != 0,
            ],
            im10: [
                (data & (1 << 8)) != 0,
                (data & (1 << 9)) != 0
            ],
        }
    }
}

#[derive(Debug)]
enum Mode {
    /// 00 value
    Kernel,
    /// 01 value
    Supervisor,
    /// 10 value
    User,
    // 11 value
    // Invalid,
}

impl Default for Mode {
    fn default() -> Mode {
        Mode::Kernel
    }
}

impl From<u32> for Mode {
    fn from(data: u32) -> Self {
        match data {
            0b00 => Mode::Kernel,
            0b01 => Mode::Supervisor,
            0b10 => Mode:: User,
            _    => panic!("Wrong Mode specified: {:#4b}.", (data >> 3) & 0b11)
        }
    }
}

#[derive(Debug,Default)]
pub struct RegStatus {
    /// Coprocessor usablility:
    cu: [bool; 4],

    /// Low power mode status:
    rp: bool,

    /// Floating points register:
    fr: bool,

    /// Reverse endian:
    re: bool,

    /// Diagnostic status:
    ds: DiagnosticStatus,

    /// Interrupt Mask:
    im: InterruptMask,

    /// 64bit adressing in kernel mode:
    kx: bool,

    /// 64bit adressing and operations in Supervisor mode:
    sx: bool,

    /// 64bit adressing and operations in User mode:
    ux: bool,

    /// CP mode:
    ksu: Mode,

    /// Error level:
    erl: bool,

    /// Exception level:
    exl: bool,

    /// Global interrupt:
    ie: bool,
}

impl From<u32> for RegStatus {
    fn from(data: u32) -> Self {
        RegStatus {
            cu: [
                (data & (1 << 28)) != 0,
                (data & (1 << 29)) != 0,
                (data & (1 << 30)) != 0,
                (data & (1 << 31)) != 0,
            ],
            // cu[3] = (data & (1 << 31)) != 0,
            // cu[2] = (data & (1 << 30)) != 0,
            // cu[1] = (data & (1 << 29)) != 0,
            // cu[0] = (data & (1 << 28)) != 0,

            rp    : (data & (1 << 27)) != 0,

            fr    : (data & (1 << 26)) != 0,

            re    : (data & (1 << 25)) != 0,

            ds    : data.into(),
            im    : data.into(),

            kx    : (data & (1 << 7)) != 0,
            sx    : (data & (1 << 6)) != 0,
            ux    : (data & (1 << 5)) != 0,

            ksu   : ((data >> 3) & 0b11).into(),

            erl   : (data & (1 << 2)) != 0,
            exl   : (data & (1 << 1)) != 0,
            ie    : (data & (1 << 0)) != 0,
        }
    }
}
