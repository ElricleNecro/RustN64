#[derive(Debug,PartialEq)]
pub enum OpCode {
    Addi,
    Addiu,
    Andi,
    Ori,
    Lui,
    Mtc0,
    Beql,
    Bnel,
    Lw,
    Sw,
}

impl OpCode {
    pub fn get(opcode: u32) -> Option<OpCode> {
        match opcode {
            0b001000 => Some(OpCode::Addi),
            0b001001 => Some(OpCode::Addiu),
            0b001100 => Some(OpCode::Andi),
            0b001101 => Some(OpCode::Ori),
            0b001111 => Some(OpCode::Lui),
            0b010000 => Some(OpCode::Mtc0),
            0b010100 => Some(OpCode::Beql),
            0b010101 => Some(OpCode::Bnel),
            0b100011 => Some(OpCode::Lw),
            0b101011 => Some(OpCode::Sw),
            _ => None,
        }
    }
}
