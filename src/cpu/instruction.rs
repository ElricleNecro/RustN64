use std::fmt;

use errors::{Error, ErrorKind};
use super::opcode::OpCode;

#[derive(Clone, Copy)]
pub struct Instruction(u32);

impl Instruction {
    #[inline(always)]
    pub fn opcode(&self) -> Result<OpCode, Error> {
        // TODO
        // (word >> 26) & 0b111111
        // OpCode::Andi
        // match OpCode::from_u32((self.0 >> 26) & 0b111111) {
        let data = (self.0 >> 26) & 0b111111;
        match OpCode::get(data) {
            Some(x) => Ok(x),
            None => Err(
                Error::new(
                    &format!("Unsupported opcode: {:#08b} {:#x}, {:#x}, {:#x} (word: {:#x}).",
                        (self.0 >> 26) & 0b111111,
                        self.rs(),
                        self.rt(),
                        self.imm(),
                        self.0
                    ),
                    ErrorKind::UnsupportedInstruction
                )
            ),
        }
    }

    #[inline(always)]
    pub fn rs(&self) -> usize {
        ((self.0 >> 21) & 0b11111) as usize
    }

    #[inline(always)]
    pub fn rt(&self) -> usize {
        ((self.0 >> 16) & 0b11111) as usize
    }

    #[inline(always)]
    pub fn rd(&self) -> u32 {
        (self.0 >> 11) & 0b11111
    }

    #[inline(always)]
    pub fn imm(&self) -> u32 {
        self.0 & 0xffff
    }

    #[inline(always)]
    pub fn imm_sign_extended(&self) -> u64 {
        (self.imm() as i16) as u64
    }

    #[inline(always)]
    pub fn offset_sign_extended(&self) -> u64 {
        (self.imm() as i16) as u64
    }

    #[inline(always)]
    pub fn offset(&self) -> u32 {
        self.imm()
    }
}

impl From<u32> for Instruction {
    fn from(v: u32) -> Self {
        Instruction(v)
    }
}

impl fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.opcode() {
            Ok(x) => write!(f, "{:#?} {:#x}, {:#x}, {:#x}", x, self.rs(), self.rt(), self.imm()),
            Err(_) => write!(f, "UNKNOWN {:#x}, {:#x}, {:#x}", self.rs(), self.rt(), self.imm()),
        }
    }
}

impl fmt::Debug for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.opcode() {
            Ok(x) => write!(f, "{:#?} {:#x}, {:#x}, {:#x}", x, self.rs(), self.rt(), self.imm()),
            Err(_) => write!(f, "UNKNOWN {:#x}, {:#x}, {:#x}", self.rs(), self.rt(), self.imm()),
        }
    }
}
