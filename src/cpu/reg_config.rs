#[derive(Debug)]
/// Enum representing the transfer data pattern.
enum TransferMode {
    /// Normal pattern:
    D,
    /// Double word (6 cycle) transfer:
    DxxDxx,
    /// All others mode:
    RFU,
}

impl Default for TransferMode {
    fn default() -> Self {
        TransferMode::D
    }
}

impl From<u32> for TransferMode {
    fn from(data: u32) -> Self {
        match (data >> 24) & 0b1111 {
            0 => TransferMode::D,
            6 => TransferMode::DxxDxx,
            _ => TransferMode::RFU,
        }
    }
}

#[derive(Debug)]
/// Endianness of the Coprocessor:
enum Endianness {
    LittleEndian,
    BigEndian,
}

impl Default for Endianness {
    fn default() -> Self {
        Endianness::BigEndian
    }
}

impl From<u32> for Endianness {
    fn from(data: u32) -> Self {
        match (data >> 15) & 0b1 {
            0 => Endianness::LittleEndian,
            1 => Endianness::BigEndian,
            _ => unreachable!(),
        }
    }
}

#[derive(Default, Debug)]
pub struct RegConfig {
    /// Operating frequency ratio:
    ec: u8,
    /// Transfer data pattern:
    ep: TransferMode,
    /// Endianness:
    be: Endianness,
    /// Reserved for future used:
    cu: bool,
    /// Coherency algorithm of kseg0 (cache settings):
    k0: bool,
}

impl From<u32> for RegConfig {
    fn from(data: u32) -> Self {
        RegConfig {
            ec: 0b110,

            ep: data.into(),

            be: data.into(),

            cu: (data & (1 << 3)) != 0,

            k0: (data & 0b111) != 0b010,
        }
    }
}
