use super::reg_config::RegConfig;
use super::reg_status::RegStatus;

const NUM_REG      : usize = 32;

const INDEX        : usize = 0;
const RANDOM       : usize = 1;
const ENTRYLO0     : usize = 2;
const ENTRYLO1     : usize = 3;
const CONTEXT      : usize = 4;
const PAGEMASK     : usize = 5;
const WIRED        : usize = 6;
const BADVADDR     : usize = 8;
const COUNT        : usize = 9;
const ENTRYHI      : usize = 10;
const COMPARE      : usize = 11;
const STATUS       : usize = 12;
const CAUSE        : usize = 13;
const EPC          : usize = 14;
const PRID         : usize = 15;
const CONFIG       : usize = 16;
const LLADDR       : usize = 17;
const WATCHLO      : usize = 18;
const WATCHHI      : usize = 19;
const XCONTEXT     : usize = 20;
const PARITY_ERROR : usize = 26;
const CACHE_ERROR  : usize = 27;
const TAGLO        : usize = 28;
const TAGHI        : usize = 29;
const ERROREPC     : usize = 30;

#[derive(Default, Debug)]
pub(crate) struct Cp0 {
    reg: [u64; NUM_REG],

    config: RegConfig,
    status: RegStatus,
}

impl Cp0 {
    pub fn write_reg(&mut self, idx: usize, data: u64) {
        self.reg[idx] = data;

        match idx {
            12 => {
                self.status = (data as u32).into();
            },
            16 => {
                self.config = (data as u32).into();
            },
            _ => {
                unimplemented!("Cp0 register {}", idx);
            },
        };
    }
}
