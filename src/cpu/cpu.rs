/// See VR4300 datasheet for more details on what registers and value mean or are meant for.
use interconnect as ic;

use super::cp0::Cp0;
use super::instruction::Instruction;
use super::opcode::OpCode::*;
use errors::Error;

use std::fmt;

const NUM_GPR: usize = 32;
const NUM_FPR: usize = 32;

pub struct Cpu {
    gpr: [u64; NUM_GPR],
    fpr: [u64; NUM_FPR],

    pc: u64,

    hi: u64,
    lo: u64,

    llbit: bool,
    fcr0: u32,
    fcr31: u32,

    cp0: Cp0,

    conn: ic::Interconnect,
}

impl Cpu {
    pub fn new(interconnect: ic::Interconnect) -> Cpu {
        Cpu {
            gpr: [0; NUM_GPR],
            fpr: [0; NUM_FPR],

            pc: 0xffff_ffff_bfc0_0000,

            hi: 0,
            lo: 0,

            llbit: false,
            fcr0: 0,
            fcr31: 0,

            cp0: Cp0::default(),

            conn: interconnect,
        }
    }

    fn virt_2_phys(&self, virt: u64) -> u64 {
        // See Table 5-3 in the VR4300 user manual.
        let bit_value = virt >> 29 & 0b111;

        if bit_value == 0b101 {
            // Dealing with kseg1:
            virt -  0xffff_ffff_a000_0000
        } else {
            // TODO: Support all memory range.
            panic!("Unsupported memory segment {:#x}.", virt);
        }
    }

    fn read_word(&self, addr: u64) -> Result<u32, Error> {
        let phys_addr = self.virt_2_phys(addr);
        self.conn.read_word(phys_addr as u32)
    }

    fn write_word(&mut self, addr: u64, word: u32) -> Result<(), Error> {
        let phys_addr = self.virt_2_phys(addr);
        self.conn.write_word(phys_addr as u32, word)
    }

    fn read_reg_gpr(&self, reg: usize) -> u64 {
        self.gpr[reg]
    }

    fn write_reg_gpr(&mut self, index: usize, value: u64) {
        if index != 0 {
            self.gpr[index] = value;
        }
    }

    fn read_instruction(&self, pc: u64) -> Result<Instruction, Error> {
        Ok(self.read_word(pc)?.into())
    }

    #[inline]
    fn branch<L>(&mut self, word: Instruction, test: L) where L: FnOnce(u64, u64) -> bool {
        let rs = self.read_reg_gpr(word.rs());
        let rt = self.read_reg_gpr(word.rt());

        if test(rs, rt) {
            let old_pc = self.pc;
            let sign_offset = word.imm_sign_extended() << 2; // Pattern to get the sign-extend, shifted left by 2

            self.pc = old_pc.wrapping_add(sign_offset);

            match self.read_instruction(old_pc) {
                Ok(delay_slot_instr) => match self.exec(delay_slot_instr) {
                    // Err(e) => return Err(e),
                    Err(_) => {},
                    Ok(_) => {},
                },
                Err(_) => {},
            }
            // let delay_slot_instr = self.read_instruction(old_pc)?;
            // match self.exec(delay_slot_instr) {
                // Err(e) => return Err(e),
                // Ok(_) => {},
            // };
        } else {
            self.pc = self.pc.wrapping_add(4);
        }
    }

    fn exec(&mut self, word: Instruction) -> Result<(), Error> {
        match word.opcode()? {
            // ADDI
            Addi => {
                // TODO: Handle exception for Addi
                let sign_ext_imm = word.imm_sign_extended();
                let value = self.read_reg_gpr(word.rs()) + sign_ext_imm;
                self.write_reg_gpr(word.rt(), value);
            },

            // ADDIU
            Addiu => {
                let sign_ext_imm = word.imm_sign_extended();
                let value = self.read_reg_gpr(word.rs()).wrapping_add(sign_ext_imm);
                self.write_reg_gpr(word.rt(), value);
            },

            // ANDI
            Andi => {
                let res = self.read_reg_gpr(word.rs() as usize) & (word.imm() as u64);
                self.write_reg_gpr(word.rt() as usize, res);
            },

            // ORI
            Ori => {
                let res = self.read_reg_gpr(word.rs() as usize) | (word.imm() as u64);
                self.write_reg_gpr(word.rt() as usize, res);
            },

            // LUI:
            Lui => {
                let value = ((word.imm() << 16) as i32) as u64; // Sign extending word.imm().
                self.write_reg_gpr(word.rt() as usize, value);
            },

            // MTC0
            Mtc0 => {
                let rd = word.rd();
                let data = self.read_reg_gpr(word.rt() as usize);
                self.cp0.write_reg(rd as usize, data);
            },

            // LW
            Lw => {
                // base -> rs
                // offset -> imm
                let sign_offset = (word.imm() as i16) as u64; // Pattern to get the sign-extend.
                let virt_addr   = self.read_reg_gpr(word.rs() as usize).wrapping_add(sign_offset);
                let mem         = (self.read_word(virt_addr)? as i32) as u64;
                self.write_reg_gpr(word.rt() as usize, mem);
            },

            // BEQL
            Beql => self.branch(word, |rs, rt| rs == rt),

            // BNEL
            Bnel => self.branch(word, |rs, rt| rs != rt),

            // Sw
            Sw => {
                let vaddr = self.read_reg_gpr(word.rs()).wrapping_add(word.offset_sign_extended());
                let save  = self.read_reg_gpr(word.rt() as usize);
                match self.write_word(vaddr, save as u32) {
                    Err(e) => return Err(e),
                    Ok(_) => {},
                };
            },
        };

        Ok(())
    }

    pub fn run_single(&mut self) -> Result<(), Error> {
        let word = self.read_instruction(self.pc)?;

        println!("{:#018x}: {}", self.pc, word);

        self.pc += 4;

        self.exec(word)
    }

    pub fn run(&mut self) -> Result<(), Error> {
        loop {
            self.run_single()?
        }
    }
}

impl fmt::Debug for Cpu {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        const REGS_PER_LINE: usize = 2;
        const REG_NAMES: [&'static str; NUM_GPR] = [
        "r0", "at", "v0", "v1", "a0", "a1", "a2", "a3",
        "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7",
        "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7",
        "t8", "t9", "k0", "k1", "gp", "sp", "s8", "ra",
        ];

        try!(write!(f, "\nCPU General Purpose Registers:"));
        for reg_num in 0..NUM_GPR {
            if (reg_num % REGS_PER_LINE) == 0 {
                try!(writeln!(f, ""));
            }
            try!(write!(f,
                "{reg_name}/gpr{num:02}: {value:#018X} ",
                num = reg_num,
                reg_name = REG_NAMES[reg_num],
                value = self.gpr[reg_num],
            ));
        }

        try!(write!(f, "\n\nCPU Floating Point Registers:"));
        for reg_num in 0..NUM_FPR {
            if (reg_num % REGS_PER_LINE) == 0 {
                try!(writeln!(f, ""));
            }
            try!(write!(f,
                "fpr{num:02}: {value:21} ",
                num = reg_num,
                value = self.fpr[reg_num]));
        }

        try!(writeln!(f, "\n\nCPU Special Registers:"));
        try!(writeln!(f,
            "\
            reg_pc: {:#018X}\n\
            reg_hi: {:#018X}\n\
            reg_lo: {:#018X}\n\
            reg_llbit: {}\n\
            reg_fcr0:  {:#010X}\n\
            reg_fcr31: {:#010X}\n\
            ",
            self.pc,
            self.hi,
            self.lo,
            self.llbit,
            self.fcr0,
            self.fcr31
        ));

        writeln!(f, "{:#?}", self.cp0)
    }
}
