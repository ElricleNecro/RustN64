use cpu::Cpu;
use errors::Error;
use super::interconnect::Interconnect;

///The N64 system itself.
#[derive(Debug)]
pub struct N64 {
    cpu: Cpu,
}

impl N64 {
    pub fn new(pif: Box<[u8]>) -> N64 {
        N64 {
            cpu: Cpu::new(Interconnect::new(pif)),
        }
    }

    pub fn run_single(&mut self) -> Result<(), Error> {
        self.cpu.run_single()
    }

    pub fn run(&mut self) -> Result<(), Error> {
        self.cpu.run()
    }
}
