#![feature(box_syntax)]

extern crate byteorder;

use std::env;
use std::io::Read;
use std::fs::File;
use std::path::Path;

mod cpu;
mod errors;
mod n64;
mod interconnect;
mod mem_map;
mod rsp;
mod peripheral_interface;

use errors::Error;
use n64::N64;

fn read_bin<P: AsRef<Path>>(path: P) -> Result<Box<[u8]>, std::io::Error> {
    let mut file = File::open(path)?;
    let mut buf = Vec::new();
    file.read_to_end(&mut buf)?;

    Ok(buf.into_boxed_slice())
}

fn main() -> Result<(), Error> {
    let pif_fname = env::args().nth(1).unwrap();
    let rom_fname = env::args().nth(2).unwrap();

    let pif = read_bin(pif_fname).unwrap();
    let _rom = read_bin(rom_fname).unwrap();

    let mut n64 = N64::new(pif);
    // n64.run();

    loop {
        n64.run_single()?
    }
}
